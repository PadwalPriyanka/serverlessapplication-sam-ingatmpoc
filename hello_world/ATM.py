from bson.objectid import ObjectId


class ATM(object):

    def __init__(self, street=None, housenumber=None, postalcode=None, city=None, lat=None, lng=None, distance=None, typee=None):
        self.street = street
        self.housenumber = housenumber
        self.postalcode = postalcode
        self.city = city
        self.lat = lat
        self.lng = lng
        self.distance = distance
        self.type = typee

    def get_as_json(self):
        """ Method returns the JSON representation of the ATM object, which can be saved to MongoDB """
        data =  {
                    "address":{
                        "street":self.street,
                        "housenumber":self.housenumber,
                        "postalcode":self.postalcode,
                        "city":self.city,
                        "geoLocation":{
                            "lat": self.lat,
                            "lng": self.lng
                        }
                    },
                    "distance":self.distance,
                    "type":self.type
                }
        #return self.__dict__
        return data

    @staticmethod
    def build_from_json(json_data):
        """ Method used to build ATM objects from JSON data returned from MongoDB """
        if json_data is not None:
            try:
                return ATM(json_data['address']['street'],
                           json_data['address']['housenumber'],
                           json_data['address']['postalcode'],
                           json_data['address']['city'],
                           json_data['address']['geoLocation']['lat'],
                           json_data['address']['geoLocation']['lng'],
                           json_data['distance'],
                           json_data['type'])
            except KeyError as e:
                raise Exception("Key not found in json_data ",e)
                #raise Exception("Key not found in json_data: {}".format(e.message))
            else:
                raise Exception("No data to create ATM from!")
