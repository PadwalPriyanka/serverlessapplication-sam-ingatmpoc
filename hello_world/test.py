import json
import requests
from pymongo import MongoClient
from ATM import ATM
from ATMRepository import ATMRepository


repository = ATMRepository()
def external_ATMList(atmList,city=None):
    external_json = requests.get('https://www.ing.nl/api/locator/atms/')

    ''' Data is incorrect removing startting character '''
    apiResponse = external_json.text[6:]
    atmListJSON = json.loads(apiResponse)
    try:
        for atm in atmListJSON:
            if atm["address"]["city"] == city:
                atmList.append(atm)

        ''''Store Data in the MongoDb'''
        repository.insert_many(atmList)     
        print("Result", atmList)
        return atmList        
    except AttributeError as error:
        print("Get MongoDB database and collection ERROR:", error)
             

def get_all_atmlists():
    try:
        atmList = list()
        city = "Kampen"
        
        atmListJSON = repository.read()
        if (atmListJSON.count() > 0):
            atmListJSON = repository.read(city)
            for atm in atmListJSON:
                if atm["address"]["city"] == city:
                    atmList.append(atm)
            print("Result", atmList)
            
            if atmList == []:
                atmList = external_ATMList(atmList,city)
                return{
                        "statusCode": 200,
                        "body": json.dumps(atmList)
                    }       
            return{
                    "statusCode": 200,
                    "body": json.dumps(atmList)
                }
        else:
            atmList = external_ATMList(atmList,city)
            return{
                    "statusCode": 200,
                    "body": json.dumps(atmList)
            }
                     


    except AttributeError as error:
        print("Get MongoDB database and collection ERROR:", error)
    
get_all_atmlists()
        
    

