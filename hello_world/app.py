import json
import requests
from pymongo import MongoClient
from ATM import ATM
from ATMRepository import ATMRepository


repository = ATMRepository()
def external_ATMList(atmList,city=None):
    external_json = requests.get('https://www.ing.nl/api/locator/atms/')

    ''' Data is incorrect removing startting character '''
    apiResponse = external_json.text[6:]
    atmListJSON = json.loads(apiResponse)
    try:
        for atm in atmListJSON:
            if atm["address"]["city"] == city:
                atmList.append(atm)

        ''''Store Data in the MongoDb'''
        repository.insert_many(atmList)     
        print("Result", atmList)
        return atmList        
    except AttributeError as error:
        print("Get MongoDB database and collection ERROR:", error)
             
def get_all_atmlists(event,context):
    try:
        atmList = list()
        #city = "Kampen"
        city = event['body']['city']
        atmListJSON = repository.read()
        if (atmListJSON.count() > 0):
            atmListJSON = repository.read(city)
            for atm in atmListJSON:
                if atm["address"]["city"] == city:
                    atmList.append(atm)
            print("Result", atmList)
            
            if atmList == []:
                atmList = external_ATMList(atmList,city)
                return{
                        "statusCode": 200,
                        "body": json.dumps(atmList)
                    }       
            return{
                    "statusCode": 200,
                    "body": json.dumps(atmList)
                }
        else:
            atmList = external_ATMList(atmList,city)
            return{
                    "statusCode": 200,
                    "body": json.dumps(atmList)
            }
                     


    except AttributeError as error:
        print("Get MongoDB database and collection ERROR:", error)
    








def lambda_handler(event, context):
    """Sample pure Lambda function

    Parameters
    ----------
    event: dict, required
        API Gateway Lambda Proxy Input Format

        Event doc: https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html#api-gateway-simple-proxy-for-lambda-input-format

    context: object, required
        Lambda Context runtime methods and attributes

        Context doc: https://docs.aws.amazon.com/lambda/latest/dg/python-context-object.html

    Returns
    ------
    API Gateway Lambda Proxy Output Format: dict

        Return doc: https://docs.aws.amazon.com/apigateway/latest/developerguide/set-up-lambda-proxy-integrations.html
    """

    # try:
    #     ip = requests.get("http://checkip.amazonaws.com/")
    # except requests.RequestException as e:
    #     # Send some context about this error to Lambda Logs
    #     print(e)

    #     raise e

    return {
        "statusCode": 200,
        "body": json.dumps({
            "message": "hello world",
            # "location": ip.text.replace("\n", "")
        }),
    }

