from pymongo import MongoClient
from bson.objectid import ObjectId

class ATMRepository(object):
    """ Repository implementing CRUD operations on atm collection in MongoDB """

    def __init__(self):
        self.client = MongoClient(host='ec2-35-154-173-205.ap-south-1.compute.amazonaws.com', port=27017)
        self.database = self.client['ATMPOC']

    def create(self, atm):
        if atm is not None:
            self.database.ATMList.insert(atm.get_as_json())            
        else:
            raise Exception("Nothing to save, because atm parameter is None")

    def read(self, city=None):
        if city is None:
            return self.database.ATMList.find({},{ "_id": 0 } )
        else:
            return self.database.ATMList.find( { "address.city": city }, { "_id": 0 } )

    def insert_one(self, atmList):
        if atmList is not None:
            self.database.ATMList._insert(atmList)            
        else:
            raise Exception("Nothing to save, because atm parameter is None")

    def insert_many(self, atmList):
        if atmList is not None:
            self.database.ATMList.insert_many(atmList)            
        else:
            raise Exception("Nothing to save, because atm parameter is None")